package npcompete.telecorp;


/**
 * Created by Luis Arriaga on 1/27/2015.
 */
public class Members {
    private int _id;
    private String _name;
    private String _email;
    private String _pass;

    public Members(){

    }

    public Members(String name, String email, String pass) {
        this._name = name;
        this._email = email;
        this._pass = pass;

    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public void set_pass(String _pass) {
        this._pass = _pass;
    }

    public int get_id() {
        return _id;
    }

    public String get_name() {
        return _name;
    }

    public String get_email() {
        return _email;
    }

    public String get_pass() {
        return _pass;
    }
}
